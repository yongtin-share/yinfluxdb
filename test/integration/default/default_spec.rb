describe command("curl -I localhost:8083") do
  its(:stdout) { should match /^HTTP\/1.1 200 OK/ }
end

describe command("curl -G localhost:8086/query --data-urlencode 'q=SHOW DATABASES'") do 
  its(:stdout) { should match /"results":/ }
end
