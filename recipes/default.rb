#
# Cookbook Name:: yinfluxdb
# Recipe:: default
#
# Copyright (c) 2016 Tsang Yong, All Rights Reserved.

package "curl"

# influxdb
node.default["influxdb"]["config_file_path"] = "/etc/influxdb.conf"

include_recipe "influxdb::default"

influxdb_admin node["yinfluxdb"]["admin"]["user"] do
  password node["yinfluxdb"]["admin"]["password"]
  action :create
end

node["yinfluxdb"]["databases"].each do |database_name, dbinfo|
  influxdb_database database_name do
    action :create
  end
  influxdb_user dbinfo["user"] do
    password dbinfo["password"]
    databases [database_name]
    action :create
  end
  influxdb_retention_policy "#{database_name}_policy" do
    policy_name dbinfo["retention_policy_name"]
    database database_name
    duration dbinfo["retention_duration"]
    replication dbinfo["replication"]
    action :create
  end
end
