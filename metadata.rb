name 'yinfluxdb'
maintainer 'Tsang Yong'
maintainer_email 'tsang.yong@gmail.com'
license 'all_rights'
description 'Installs/Configures yinfluxdb'
long_description 'Installs/Configures yinfluxdb'
version '0.1.0'

depends "influxdb"
