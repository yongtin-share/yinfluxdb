default.yinfluxdb.admin.user = "test_admin"
default.yinfluxdb.admin.password = "test_admin_password"
default.yinfluxdb.databases = {
  "test_database" => {
    "user" => "test_user", 
    "password" => "test_password",
    "retention_policy_name" => "default",
    "retention_duration" => "1w",
    "replication" => 1
  }
}
